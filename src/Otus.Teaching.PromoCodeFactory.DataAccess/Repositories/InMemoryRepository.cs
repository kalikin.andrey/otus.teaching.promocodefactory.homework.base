﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
// ReSharper disable InvertIf
#pragma warning disable 1998

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (data == null) throw new NullReferenceException(nameof(data));
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<T> CreateAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            for (var i = 0; i < Data.Count; i++)
            {
                if (Data[i].Id == entity.Id)
                {
                    Data[i] = entity;
                    return entity;
                }
            }
            throw new Exception($"Entity with id {entity.Id} does not exist");
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            return Data.RemoveAll(x => x.Id == id) > 0;
        }
    }
}