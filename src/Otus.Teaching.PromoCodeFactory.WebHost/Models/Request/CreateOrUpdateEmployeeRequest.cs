using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class CreateOrUpdateEmployeeRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }


        public string Email { get; set; }

        public List<string> Roles { get; set; } = new List<string>();

        public int AppliedPromocodesCount { get; set; }
    }
}