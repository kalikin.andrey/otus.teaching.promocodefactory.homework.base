using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public interface IEmployeeMapper
    {
        EmployeeResponse ToEmployeeResponse(Employee employee);
        Task<Employee> ToEmployeeAsync(CreateOrUpdateEmployeeRequest employeeRequest);
    }
    
    public class EmployeeMapper : IEmployeeMapper
    {
        private readonly IRepository<Employee> _repository;
        public EmployeeMapper(IRepository<Employee> repository)
        {
            _repository = repository;
        }
        
        public async Task<Employee> ToEmployeeAsync(CreateOrUpdateEmployeeRequest employeeRequest)
        {
            if (employeeRequest == null) throw new NullReferenceException(nameof(employeeRequest));

            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Email = employeeRequest.Email
            };

            return employee;
        }
        
        public EmployeeResponse ToEmployeeResponse(Employee employee)
        {
            if (employee == null) return null;
            
            return new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
    }
}