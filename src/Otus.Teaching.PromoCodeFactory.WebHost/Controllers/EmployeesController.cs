﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IEmployeeMapper _mapper;
        
        public EmployeesController(IRepository<Employee> employeeRepository, IEmployeeMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = _mapper.ToEmployeeResponse(employee);
            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody] CreateOrUpdateEmployeeRequest employeeRequest)
        {
            if (employeeRequest == null) return BadRequest("Could not create employee with null value");

            try
            {
                var employee = await _mapper.ToEmployeeAsync(employeeRequest);
                var createdEmployee = await _employeeRepository.CreateAsync(employee);
                return _mapper.ToEmployeeResponse(createdEmployee);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        
        /// <summary>
        /// Обновить данные о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync([FromBody] Employee employee)
        {
            if (employee == null) return BadRequest("Could not update employee with null value");

            try
            {
                return _mapper.ToEmployeeResponse(await _employeeRepository.UpdateAsync(employee));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        
        /// <summary>
        /// Удалить данные о сотруднике по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> DeleteEmployeeAsync(Guid id)
        {
            try
            {
                return await _employeeRepository.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}